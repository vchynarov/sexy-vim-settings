" Vundle configuration START

set nocompatible              " be iMproved, required
filetype off                  " required

"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')


" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" My Plugins
" *********************************
" Jellybeans colourscheme
Plugin 'nanotech/jellybeans.vim'
" Git integration
Plugin 'tpope/vim-fugitive'
" Ctrlp fuzzy-finder
Plugin 'kien/ctrlp.vim'

" Language settings.
Plugin 'pangloss/vim-javascript'

Plugin 'mxw/vim-jsx'
Plugin 'derekwyatt/vim-scala'

" Airline status bar
Plugin 'bling/vim-airline'

" Themes for vim-airline
Plugin 'vim-airline/vim-airline-themes'

" Multiple cursors
Plugin 'terryma/vim-multiple-cursors'

" Dependency Used for neocomplete (autocomplete)
Plugin 'L9'
" Autocompletion
Plugin 'Shougo/neocomplete.vim'

" Rainbow parentheses
Plugin 'kien/rainbow_parentheses.vim'

" Tagbar
Plugin 'majutsushi/tagbar'

" Autoformatter
Plugin 'Chiel92/vim-autoformat'
            

" Ack searching
Plugin 'mileszs/ack.vim'
" Ag (ignores gitignore automatically)
Plugin 'rking/ag.vim'

" Nerdtree for plugin browsing
Plugin 'scrooloose/nerdtree'
" Session management
" Plugin 'xolox/vim-misc'
" Plugin 'xolox/vim-session'
"
Plugin 'mkitt/tabline.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
" Vundle configuration END
" ***********************************

" Settings for NERD
" Needed because of font set might not include the default arrows, this works for sure.
let NERDTreeDirArrows=0
map <c-o> :NERDTreeToggle <enter>

let g:NERDTreeDirArrowExpandable = '>'
let g:NERDTreeDirArrowCollapsible = 'v'
syntax on

filetype plugin indent on    " required
"SPACING OPTIONS
set tabstop=2
set expandtab
set shiftwidth=2
set autoindent
set ruler
set number
" Backspace is actually kind of fucked up.
set backspace=indent,eol,start
"set colorcolumn=100

" Neocomplete settings (autocompletion)
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_auto_select = 1

" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  " Allows pressing enter to complete without going to new line.
  return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
" End neocomplete settings.

" Better editing options
" Hm, don't like anymore. map <c-y> :redo<enter>
map ; :RainbowParenthesesToggleAll<enter>

"I set these custom options for better tab navigation
" noremap is used to mean 'norecursivemap' so it doesn't tke other 
" mappings into account. However it's only useful for other keys.
" gt is default, GT is an easier mapping.
noremap gt :tabn<enter>
noremap GT :tabp<enter>
noremap <c-t> :tabnew<enter>
noremap <C-S> :w<enter>

" Useful for my tag listing
nmap <F8> :TagbarToggle<CR>

" highlight last searches
set hlsearch
" jumps to search locations.
set incsearch
" escape clears highlighting
" nnoremap <esc> :noh<return><esc>

" First line ignores VCS directories and webdev download folders.
" Second line ignores dist or build directories.
" Third line ignores certain file extensions. 
"let g:ctrlp_user_command = '
"\ find %s | 
"\ egrep -v "^.+(\.(git|hg|svn)|bower_components|node_modules)/.+$" |
"\ egrep -v "^.+/(dist|build)/.+$" | 
"\ egrep -v "^.+\.(min\.js|o|pyc|beam|jpg|gif|png|ttf|woff|gzip|zip)$" | 
"\ egrep "^.+/.+\.[a-zA-Z0-9]{1,5}$"'
let g:ctrlp_switch_buffer = 'E'

set modelines=0
set cursorline
set ttyfast
set wildmenu
set mouse=a

" The sexiest dark background ever.
colorscheme jellybeans
" Nice colour for tag line
let g:airline_theme='badwolf'

"always show last statusline
set laststatus=2

" I use these settings to remove backup since everything's in version
" control anyway and this reduces clutter and shit.
" Turns backup offf.
set nobackup
set nowb
set noswapfile

" Autoloading file changes externally (edited with different editor / Git checkout)
set autoread
" set updatetime=1000
" autocmd CursorHold * :e
" Before insert enter, read the file
" Update the file when insert mode is changed, or when switching tabs.
autocmd InsertEnter * :checktime
autocmd InsertLeave * :checktime 
autocmd BufEnter * :checktime " Update all tabs
